using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPiso : MonoBehaviour
{
    private int hp;
    private GameObject jugador;

    public void recibirDaño()
    {
        hp = hp - 1000000;

        if (hp <= 0)
        {
            this.desaparecer();
        }
    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("piso"))
        {
            recibirDaño();
        }
    }

}
